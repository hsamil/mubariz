# Coded By GitHub/hsamil TG/hsamil #
# Don't kang without permission #
# @MubarizUserBot #


import asyncio
import os
import sys
import time
import random
import subprocess

try:
   from telethon import TelegramClient, events, version
   from telethon.errors import SessionPasswordNeededError, PhoneCodeInvalidError, PasswordHashInvalidError, PhoneNumberInvalidError
   from telethon.network import ConnectionTcpAbridged
   from telethon.utils import get_display_name
   from telethon.sessions import StringSession
except:
   subprocess.check_call([sys.executable, "-m", "pip", "install", 'telethon'])   
finally:
   from telethon import TelegramClient, events, version
   from telethon.errors import SessionPasswordNeededError, PhoneCodeInvalidError, PasswordHashInvalidError, PhoneNumberInvalidError
   from telethon.network import ConnectionTcpAbridged
   from telethon.utils import get_display_name
   from telethon.sessions import StringSession

try:
   import requests
   import bs4
except:
   print("[!] Requests Tapılmadı. Yüklənir...")
   print("[!] Bs4 Tapılmadı. Yüklənir...")

   subprocess.check_call([sys.executable, "-m", "pip", "install", 'requests'])
   subprocess.check_call([sys.executable, "-m", "pip", "install", 'bs4'])
finally:
   import requests
   import bs4

os.system("clear")
def hata (text):
   print("\u001b[31m" + text + "\u001b[0m")
def bilgi (text):
   print("\u001b[34m" + text + "\u001b[0m")
def onemli (text):
   print("\u001b[36;1m" + text + "\u001b[0m\u001b[0m")
def soru (soru):
   return input("\u001b[33m" + soru + "\u001b[0m")

loop = asyncio.get_event_loop()

class InteractiveTelegramClient(TelegramClient):
    # Original Source https://github.com/LonamiWebs/Telethon/master/telethon_examples/interactive_telegram_client.py #

    def __init__(self, session_user_id, api_id, api_hash,
                 telefon=None, proxy=None):
        super().__init__(
            session_user_id, api_id, api_hash,
            connection=ConnectionTcpAbridged,
            proxy=proxy
        )
        self.found_media = {}
        bilgi('[i] Telegramın Serverlərinə qoşulma...')
        try:
            loop.run_until_complete(self.connect())
        except IOError:
            hata('[!] Bağlanarkən xəta baş verdi. Yenidən cəhd edirəm...')
            loop.run_until_complete(self.connect())

        if not loop.run_until_complete(self.is_user_authorized()):
            if telefon == None:
               user_phone = soru('[?] Telefon Numaranız (Örnek: +994xxxxxxxxxx): ')
            else:
               user_phone = telefon
            try:
                loop.run_until_complete(self.sign_in(user_phone))
                self_user = None
            except PhoneNumberInvalidError:
                hata("[!] Yanlış bir nömrə daxil etmisiniz, Nümunə olaraq daxil edin. Nümunə: +994xxxxxxxxxx")
                exit(1)
            except ValueError:
               hata("[!] Yanlış bir nömrə daxil etmisiniz, Nümunə olaraq daxil edin. Nümunə: +994xxxxxxxxxx")
               exit(1)

            while self_user is None:
               code = soru('[?] Telegramdan Beş (5) rəqəmli kod daxil edin: ')
               try:
                  self_user =\
                     loop.run_until_complete(self.sign_in(code=code))
               except PhoneCodeInvalidError:
                  hata("[!] Kodu səhv yazdınız. Zəhmət olmasa bir daha cəhd edin. [Təcrübə sizi qadağan edəcəkdir]")
               except SessionPasswordNeededError:
                  bilgi("[i] İki mərhələli doğrulama aşkar edildi.")
                  pw = soru('[?] Şifrənizi daxil edin: ')
                  try:
                     self_user =\
                        loop.run_until_complete(self.sign_in(password=pw))
                  except PasswordHashInvalidError:
                     hata("[!] 2 Mərhələli Parolunuzu Yanlış Yazdınız. Zəhmət olmasa bir daha cəhd edin. [Təcrübə qadağan etməyinizə səbəb olur]")


if __name__ == '__main__':
   surum = str(sys.version_info[0]) + "." + str(sys.version_info[1])
   bilgi("@MubarizUserBot String V3\nTelegram: @MubarizUserBot\nPython: " + surum + "\nTeleThon: " + version.__version__ + "\nBs4/Requests: ✅\n")
   onemli("[1] Yeni metod")
   onemli("[2] Köhnə metod\n")
   
   try:
      secim = int(soru("[?] Seçim edin [1/2]: "))
   except:
      hata("\n[!]Xahiş edirəm [1 ​​və ya 2] yazın!")
      exit(1)

   if secim == 2:
      API_ID = soru('[?] API kimliyiniz [Hazır düymələrdən istifadə etmək üçün boş buraxın]: ')
      if API_ID == "":
         bilgi("[i] Əvvəlcədən Düymələrdən istifadə...")
         API_ID = 6
         API_HASH = "eb06d4abfb49dc3eeb1aeb98ae0f581e"
      else:
         API_HASH = soru('[?] API HASH\'iniz: ')

      client = InteractiveTelegramClient(StringSession(), API_ID, API_HASH)
      bilgi("[i] Aşağıda simli açarınız var!\n\n\n" + client.session.save())
   elif secim == 1:
      numara = soru("[?] Telefon nömrəniz: ")
      try:
         rastgele = requests.post("https://my.telegram.org/auth/send_password", data={"phone": numara}).json()["random_hash"]
      except:
         hata("[!] Kod göndərilmədi. Telefon nömrənizi yoxlayın.")
         exit(1)
      
      sifre = soru("[?] Kodu Telegramdan yazın: ")
      try:
         cookie = requests.post("https://my.telegram.org/auth/login", data={"phone": numara, "random_hash": rastgele, "password": sifre}).cookies.get_dict()
      except:
         hata("[!]Kodu səhv yazdığınız ehtimalı. Xahiş edirəm Ssenarini yenidən başladın.")
         exit(1)
      app = requests.post("https://my.telegram.org/apps", cookies=cookie).text
      soup = bs4.BeautifulSoup(app, features="html.parser")

      if soup.title.string == "Create new application":
         bilgi("[i] Tətbiqiniz yoxdur. Yaradırıq...")
         hashh = soup.find("input", {"name": "hash"}).get("value")
         AppInfo = {
            "hash": hashh,
            "app_title":"Mubariz UserBot",
            "app_shortname": "MubarizUserBot",
            "app_url": "",
            "app_platform": "android",
            "app_desc": ""
         }
         app = requests.post("https://my.telegram.org/apps/create", data=AppInfo, cookies=cookie).text
         bilgi("[i] Tətbiq uğurla yaradıldı!")
         bilgi("[i] API ID/HASH almaq...")
         newapp = requests.get("https://my.telegram.org/apps", cookies=cookie).text
         newsoup = bs4.BeautifulSoup(newapp, features="html.parser")

         g_inputs = newsoup.find_all("span", {"class": "form-control input-xlarge uneditable-input"})
         app_id = g_inputs[0].string
         api_hash = g_inputs[1].string
         bilgi("[i] Məlumat gətirildi! Xahiş edirəm bunlara NOT Edin.\n")
         onemli(f"[i] API ID: {app_id}")
         onemli(f"[i] API HASH: {api_hash}\n\n")
         bilgi("[i] String alınır...")
         client = InteractiveTelegramClient(StringSession(), app_id, api_hash, numara)
         print("[i] String Açarınız Aşağıdadır!\n\n\n" + client.session.save())

      elif  soup.title.string == "App configuration":
         bilgi("[i] Artıq bir tətbiq yaratmısınız. API ID/HASH Çıxarmaq...")
         g_inputs = soup.find_all("span", {"class": "form-control input-xlarge uneditable-input"})
         app_id = g_inputs[0].string
         api_hash = g_inputs[1].string
         bilgi("[i] Məlumat gətirildi! Xahiş edirəm bunlara NOT edin.\n")
         onemli(f"[i] API ID: {app_id}")
         onemli(f"[i] API HASH: {api_hash}\n\n")
         bilgi("[i] String alınır...")

         client = InteractiveTelegramClient(StringSession(), app_id, api_hash, numara)
         print("[i] String Açarınız Aşağıdadır!\n\n\n" + client.session.save())
      else:
         hata("[!] Bir Xəta Oldu.")
         exit(1)
   else:
      hata("[!] Bilinməyən seçim.")
      exit(1)
